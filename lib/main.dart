import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirstApp());
}

class MyFirstApp extends StatelessWidget {
  const MyFirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.sailing),
          title: const Text('My First App'),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.facebook),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.favorite),
            ),
          ],
        ),
        body: Center(
          child: Column(
            children: [
              // Image.asset(
              //   'assets/images/Ja.jpg',
              //   height: 500,
              //   width: 300,
              // ),

              CircleAvatar(
                backgroundImage: AssetImage('assets/images/Ja.jpg'),
                radius: 150,
              ),
              Text(''),
              Text(
                'Sakaorat Tulaton',
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
